import wx
import wx._core

print("Initialize")


class Window(wx.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.title_stext = wx.StaticText(self, wx.ID_ANY, pos=(5, 10), label="Title: ")
        self.title_text = wx.TextCtrl(self, wx.ID_ANY, pos=(50, 5), size=(229, 24))
        self.value_stext = wx.StaticText(self, wx.ID_ANY, pos=(5, 45), label="Value: ")
        self.value_text = wx.TextCtrl(self, wx.ID_ANY, pos=(50, 40), size=(229, 104), style=wx.TE_MULTILINE)
        self.icon_error = wx.RadioButton(self, wx.ID_ANY, pos=(220, 160), label="ERROR")
        self.icon_quest = wx.RadioButton(self, wx.ID_ANY, pos=(5, 160), label="QUESTION")
        self.icon_warn = wx.RadioButton(self, wx.ID_ANY, pos=(140, 160), label="WARNING")
        self.icon_info = wx.RadioButton(self, wx.ID_ANY, pos=(87, 160), label="INFO")
        self.icon_info.SetValue(True)

        self.btn_yes = wx.CheckBox(self, wx.ID_YES, pos=(5, 180), label="YES")
        self.btn_no = wx.CheckBox(self, wx.ID_YES, pos=(47, 180), label="NO")
        self.btn_cancel = wx.CheckBox(self, wx.ID_YES, pos=(87, 180), label="CANCEL")
        self.btn_cancel = wx.CheckBox(self, wx.ID_YES, pos=(154, 180), label="OK")

        # self.icon_excl = wx.RadioButton(self, wx.ID_ANY, pos=(5, 100), label="EXCLAMATION")
        # self.icon_hand = wx.RadioButton(self, wx.ID_ANY, pos=(107, 100), label="HAND")
        self.create_btn = wx.Button(self, wx.ID_ANY, pos=(190, 205), label="Create")
        self.create_btn.Bind(wx.EVT_BUTTON, self._create)
        self.SetSizer(hbox)

    def _create(self, evt):
        title = self.title_text.GetValue()
        value = self.value_text.GetValue()
        is_error = self.icon_error.GetValue()
        is_quest = self.icon_quest.GetValue()
        is_warn = self.icon_warn.GetValue()
        is_info = self.icon_info.GetValue()
        # is_excl = self.icon_excl.GetValue()
        # is_hand = self.icon_hand.GetValue()

        has_yes = self.btn_yes.GetValue()
        has_no = self.btn_no.GetValue()
        has_cancel = self.btn_cancel.GetValue()

        if is_error:
            icon = wx.ICON_ERROR
        elif is_quest:
            icon = wx.ICON_QUESTION
        elif is_warn:
            icon = wx.ICON_WARNING
        elif is_info:
            icon = wx.ICON_INFORMATION
        # elif is_excl:
        #     icon = wx.ICON_EXCLAMATION
        # elif is_hand:
        #     icon = wx.ICON_HAND
        else:
            icon = None

        if has_yes:
            if 'a' in locals().keys():
                a = a | wx.YES
            else:
                a = wx.YES
        if has_no:
            if 'a' in locals().keys():
                a = a | wx.NO
            else:
                a = wx.NO
        if has_cancel:
            if 'a' in locals().keys():
                a = a | wx.CANCEL
            else:
                a = wx.CANCEL

        frame.Show(False)

        try:
            if 'a' in locals().keys():
                wx.MessageBox(value, title, style=icon | a)
            else:
                wx.MessageBox(value, title, style=icon)
        except wx._core.wxAssertionError:
            pass
        frame.Show(True)


print("Starting...")

if __name__ == '__main__':
    app = wx.App()
    # app.SetAppName("Error Generator")

    frame = wx.Frame(None, size=(300, 400))
    frame.SetTitle("Error Generator")
    window = Window(frame)
    window.Show()
    frame.Show()

    app.MainLoop()
    print("Exit")
